module.exports = {
    mode: 'jit',
    purge: ['./src/pages/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        fontFamily: {
            montserrat: ['Montserrat', 'sans-serif']
        },
        extend: {}
    },
    variants: {
        extend: {}
    },
    // eslint-disable-next-line global-require
    plugins: []
};
