import React from 'react';

import Layout from 'components/Layout';

const Home = () => {
    return (
        <Layout title="Home">
            <div>Content</div>
        </Layout>
    );
};

export default Home;
