import React from 'react';

import { AppProps } from 'next/app';
import '../styles/tailwind.scss';

const MyApp = ({ Component, pageProps }: AppProps) => {
    return <Component {...pageProps} />;
};

export default MyApp;
